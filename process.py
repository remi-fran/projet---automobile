import csv
import os 
import datetime

def lectureFichier(nom_fichier, delimiteur="|"):
    file_content = ""
    with open(nom_fichier) as csv_file:
        reader = csv.reader(csv_file, delimiter=delimiteur)
        file_content = [row for row in reader]
    return file_content

def ecritureFichier(nom_fichier, content, delimiteur=";"):
    nom_new_fichier = nom_fichier[:len(nom_fichier)-3] + "new.csv"
    with open(nom_new_fichier, 'w') as csv_file:
        writer = csv.writer(csvfile, delimiter=delimiteur)
        writer.writerows(content)
        return True
    return False

def modifColonnes(content):
    new_content = [[0 for i in range(19)] for j in range(len(content))]
    for i in range(1, len(content)):
        new_content[i][0] = content[i][0]
        new_content[i][1] = content[i][11]
        new_content[i][2] = content[i][8]
        new_content[i][3] = content[i][9]
        date = datetime.strptime(content[i][5], "%Y-%m-%d")
        new_content[i][4] = content[i][16]
        new_content[i][5] = content[i][10]
        new_content[i][6] = content[i][6]
        new_content[i][7] = content[i][3]
        new_content[i][8] = content[i][1]
        new_content[i][9] = content[i][2]
        new_content[i][10] = content[i][4]
        new_content[i][11] = content[i][7]
        new_content[i][12] = content[i][12]
        new_content[i][13] = content[i][13]
        new_content[i][14] = content[i][14]
        new_content[i][15] = content[i][15]
        new_content[i][16] = content[i][15].split(', ')[0]
        new_content[i][17] = content[i][15].split(', ')[1]
        new_content[i][18] = content[i][15].split(', ')[2]
new_content[0][0] = "adresse_titulaire"
new_content[0][1] = "nom"
new_content[0][2] = "prenom"
new_content[0][3] = "immatriculation"
new_content[0][4] = "date_immatriculation"
new_content[0][5] = "vin"
new_content[0][6] = "marque"
new_content[0][7] = "denomination_comerciale"
new_content[0][8] = "couleur"
new_content[0][9] = "carrosserie"
new_content[0][10] = "categorie"
new_content[0][11] = "cylindree"
new_content[0][12] = "energie"
new_content[0][13] = "places"
new_content[0][14] = "poids"
new_content[0][15] = "puissance"
new_content[0][16] = "type"
new_content[0][17] = "variante"
new_content[0][18] = "version"
return new_content
