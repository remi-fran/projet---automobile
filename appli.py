import argparse
import os 
from csvprocess import *

parser = argparse.ArgumentParser (description="Script qui permet de transformer un csv")
parser.add_argument("fichier", help="le chemin du fichier à modifier")
parser.add_argument("-o", "--output", help="Fichier cible")
parser.add_argument("-d", "--debutDelimiter", help="Délimiteur d'origine si ce n'est pas \"|\"", default="|")
parser.add_argument("-D", "--finDelimiter", help="Délimiteur cible si ce n'est pas \";\"", default=";")

args = parser.parse_args()

if (not os.path.exists(args.fichier)):
    print("Le fichier" + args.fichier + " n'existe pas")
    exit(1)

if (not os.path.isfile(args.fichier)):
    print(args.fichier + " ne correspond pas à un fichier")
    exit(2)

file_content = lectureFichier(args.fichier, args.debut_delimiter)

if file_content == None:
    print("Erreur lors de la lecture")
    exit(3)

file_content = modifColonnes(file_content)

if args.output:
    fichier_dest = args.output
else:
    fichier_dest = args.fichier

if (ecritureFichier(fichier_dest, file_content, args.fin_delimiter)):
    print("Fichier écrit")
    exit(0)
else :
    print("Erreur lors de l'écriture")
    exit(4)